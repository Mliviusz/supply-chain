const ItemManager = artifacts.require("./ItemManager.sol");
const Item = artifacts.require("./Item.sol");

contract("ItemManager", accounts => {
    it("... should be able to add an Item", async function() {
        const itemManagerInstance = await ItemManager.deployed();
        const itemName = "test1";
        const itemPrice = 500;

        // Check itemIndex
        const result = await itemManagerInstance.createItem(itemName, itemPrice, {from: accounts[0]});
        const index = result.logs[0].args._itemIndex;
        assert.equal(index, 0, "It's not the first item");

        let s_item = await itemManagerInstance.items(0);

        // Check identifier
        assert.equal(s_item._identifier, itemName, "The identifier was different");
        // Check SupplyChainStep
        assert.equal(s_item._step, 0, "The SupplyChainSteop is not 0");

        // Check itemPrice
        const itemContract = await Item.at(s_item._item);
        const priceInContract = await itemContract.priceInWei.call();
        assert.equal(priceInContract, itemPrice, "The price was different");

        // The buyer is paying for the item
        web3.eth.sendTransaction({to:s_item._item, value:itemPrice, from:accounts[1], gas:300000});

        // Check SupplyChainStep again
        s_item = await itemManagerInstance.items(0);
        assert.equal(s_item._step, 1, "The SupplyChainStep is not 1");

        //Check ItemContract after paying
        const paidWeiInContract = await itemContract.paidWei.call();
        assert.equal(paidWeiInContract, itemPrice, "The price was different");

        // Trigger Delivery by Owner
        const deliveryResult = await itemManagerInstance.triggerDelivery(index, {from:accounts[0]});
        // Check SupplyChainStep after delivering
        assert.equal(deliveryResult.logs[0].args._step, 2, "The SupplyChainStep is not 2");
    })
});