Solidity, Truffle & React Project. 
Event-Triggers / Supply Chain Example

Real-World Use-Case for this Project 

💡 Can be part of a supply-chain solution

💡 Automated Dispatch upon payment

💡 Payment collection without middlemen

Development-Goal/Showcase 

👍 Showcase Event-Triggers

📖 Workflow with Truffle

🧪 Understand Unit Testing with Truffle

🙌 Understand Events in HTML
